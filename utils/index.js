/*
   __       __               
  / /  ___ / /__  ___ _______
 / _ \/ -_) / _ \/ -_) __(_-<
/_//_/\__/_/ .__/\__/_/ /___/
          /_/                

  Helpers is a plugin. A plugin that helps.
  For Vue plugin documentation, please see : 
  https://vuejs.org/v2/guide/plugins.html#Writing-a-Plugin
*/

import filters from './filters.js';

const install = (app) => {
  /* ---------- base filters ------------- */
  const methods = {} 
  Object.keys(filters).forEach((fname) => {
    console.debug('Helpers: installing filter', fname);
    methods[fname] = filters[fname]
  })
  app.config.globalProperties.$Filter= methods
}

export default {
  install
}
