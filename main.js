import utils from './utils'


// /* Global components registering */
const requireComponent = require.context(
  // The relative path of the components folder
  '@/base-front/components',
  // Whether or not to look in subfolders
  true,
  // The regular expression used to match base component filenames
  /Base[A-Z]\w+\.(vue|js)$/
)

const initBaseComponents = (app) => {
  requireComponent.keys().forEach(fileName => {
    // Get component config
    const componentConfig = requireComponent(fileName)

    // Get PascalCase name of component
    const componentName = fileName.split('/').pop().replace(/\.\w+$/, '')

    // console.info("Registering component:", componentName)

    // Register component globally
    app.component(componentName, componentConfig.default || componentConfig)
  })
  // Register Base utils
  utils.install(app)
}

export default {
  requireComponent,
  initBaseComponents
}
