export const baseinputs = {
  data: function () {
    return {}
  },
  props: {
    /**
     * @prop {String}
     * label of field
    */
    label: {
      type: String,
      required: false,
      default: null
    },
    /**
     * @prop {Object}
     * Contains all props not declare here (external props)
     */
    externalProps: {
      type: Object,
      required: false,
      default: null
    },
    /**
     * @prop {Array}
     * General styling
     * Available styles are defined in components.
    */
    styles: {
      type: Array,
      required: false,
      default: () => {
        return [];
      },
    },
    /**
     * @prop {Boolean}
     * Field can be disabled.
    */
    disabled:{
      type: Boolean,
      required: false,
      default: false,
    },   
    /**
     * @prop {String}
     * fieldType permits field mapping by simple 'type' name.
     * Please look at fieldComponent to learn more.
    */
    fieldType: {
      type: String,
      required: false,
      default: () => {
        return '';
      },
    },
    /**
     * @prop {Boolean}
     * Required is transmitted as prop to children.
     * Is used for local validation purposes.
    */
    required: {
      type: Boolean,
      required: false,
      default: false
    },
    /**
     * @prop {String, Number}
     * max length of field
    */
    maxLength: {
      type: [String, Number],
      required: false,
      default: undefined
    },
    /**
     * @prop {String, Number}
     * Placeholder element
    */
    placeholder: {
      type: [String, Number],
      required: false,
      default: undefined
    },
    /**
     * @param {String}
     * helptext for field
    */
    helpText: {
      type: String,
      required: false,
      default: null
    },
    /**
      * @param {String}
      * autocomplete of field
     */  
    autocomplete: {
      type: String,
      required: false,
      default: undefined
    },
    /**
     * @param {Object, Array, String}
     * management of errors
    */
    errors: {
      type: [Object, Array, String],
      required: false,
      default: () => {
        return null
      }
    },
  },
  emits: ['update:value', 'valid', 'invalid'],
  computed: {
    inputListeners: function(){
      let vm = this
      return {
        input: function(event){
          vm.checkValidity(event.target.value)
          vm.$emit('update:value', event.target.value)
        }
      }
    },
  },
  methods: {
    checkValidity: function(){
      if (this.$refs.inputElement.validity.valid){
        this.$emit('valid')
      } else {
        this.$emit('invalid', this.$refs.inputElement.validationMessage)
      }
    }
  },
};
